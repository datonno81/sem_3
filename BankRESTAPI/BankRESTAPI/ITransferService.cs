﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BankRESTAPI
{
    [ServiceContract]
    public interface ITransferService
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "NewTransfer/")]
        bool NewTransfer(NewTransfer newTransfer);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "ClientGetTransferHistory/")]
        List<ClientGetTransferHistoryResult> ClientGetTransferHistory(ClientGetTransferHistory data);

        [OperationContract]
        [WebInvoke(Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "PartnerGetTransferHistory/")]
        List<PartnerGetTransferHistoryResult> PartnerGetTransferHistory(PartnerGetTransferHistory data);
    }
}
