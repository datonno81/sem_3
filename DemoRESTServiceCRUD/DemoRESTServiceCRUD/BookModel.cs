﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DemoRESTServiceCRUD
{
    [DataContract]
    public class BookModel
    {
        [DataMember]
        public int BookId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ISBN { get; set; }
    }

    public interface IBookReponsitory
    {
        List<BookModel> GetAllBook();
        BookModel GetBookById(int id);
        BookModel AddNewBook(BookModel item);
        bool UpdateBook(BookModel item);
        bool DeleteBook(int id);
    }

    public class BookReponsitory : IBookReponsitory
    {
        List<BookModel> listBook = new List<BookModel>();
        public int idCounter = 0;

        public BookReponsitory()
        {
            AddNewBook(new BookModel { BookId = 1, Title = "Hello World", ISBN = "@#11234" });
        }

        public List<BookModel> GetAllBook()
        {
            return listBook;
        }

        public BookModel GetBookById(int id)
        {
           return listBook.Find(book => book.BookId == id);
        }

        public BookModel AddNewBook(BookModel item)
        {
            try
            {
                idCounter += 1;
                item.BookId = idCounter;
                listBook.Add(item);
                return item;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateBook(BookModel item)
        {
            try
            {
                BookModel modifyBook = listBook.Find(book => book.BookId == item.BookId);
                modifyBook.Title = item.Title;
                modifyBook.ISBN = item.ISBN;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteBook(int id)
        {
            try
            {
                listBook.RemoveAll(book => book.BookId == id);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }        
}