﻿using ConsumingRESTServiceCRUD_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ConsumingRESTServiceCRUD_Client.Controllers
{
    public class BookController : Controller
    {
        public ActionResult Index()
        {
            var syncClient = new WebClient();
            var content = syncClient.DownloadString("http://localhost:54381/Book.svc" + "/Books");
            var json_serializer = new JavaScriptSerializer();
            ViewBag.listBook = json_serializer.Deserialize<List<Book>>(content);
            return View();
        }
    }
}
